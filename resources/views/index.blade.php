<!DOCTYPE html>
<html>
<head>
	<title>try</title>
</head>
<body>

	<h3>Data Film</h3>

	<a href="/cast/create"> + Tambah Cast baru</a>
	
	<br/>
	<br/>

	<table border="1">
		<tr>
			<th>Id</th>
			<th>nama</th>
			<th>umur</th>
			<th>bio</th>
			<th>action</th>
		</tr>
		@foreach($cast_film as $p)
		<tr>
			<td>{{ $p->id}}</td>
			<td>{{ $p->nama}}</td>
			<td>{{ $p->umur}}</td>
			<td>{{ $p->bio}}</td>
			<td>
				<a href="/cast/edit/{{ $p->id }}">Edit</a>
				|
				<a href="/cast/hapus/{{ $p->id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>


</body>
</html>