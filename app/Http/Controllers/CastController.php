<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
    	$cast = DB::table('cast_film')->get();
 
    	return view('index',['cast_film' => $cast]);
    }

    public function create()
    {
	    return view('create');
    }

    public function store(Request $request)
    {
	    DB::table('cast_film')->insert([
		    'nama' => $request->nama,
		    'umur' => $request->umur,
		    'bio' => $request->bio,
	    ]);
	    return redirect('/cast');

    }
    public function edit($id)
    {
	    $cast_film = DB::table('cast_film')->where('id',$id)->get();

	    return view('edit',['cast_film' => $cast_film]);
    }
    public function update(Request $request)
    {
	    DB::table('cast_film')->where('id',$request->id)->update([
		    'nama' => $request->nama,
		    'umur' => $request->umur,
		    'bio' => $request->bio
	    ]);

	    return redirect('/cast');
    }
    public function hapus($id)
    {
	    DB::table('cast_film')->where('id',$id)->delete();
		
	    return redirect('/cast');
    }

}
